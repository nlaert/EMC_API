package com.laertdev.emc.controllers;

import com.laertdev.emc.DTO.resources.RaffleResource;
import com.laertdev.emc.services.RaffleInformationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RaffleController {
    private static final Logger LOG = LoggerFactory.getLogger(RaffleController.class);
    private final RaffleInformationService raffleInformationService;

    @Value("${emc.raffle.update.password:ThisShouldHaveBeenFilled}")
    private String accessKey;

    @Autowired
    public RaffleController(RaffleInformationService raffleInformationService) {
        this.raffleInformationService = raffleInformationService;
    }

    @GetMapping("/raffle/update")
    public ResponseEntity<RaffleResource> updateRaffle(@RequestParam("key") String key) {
        if (!accessKey.equals(key)) {
            LOG.warn("Invalid key. ({})", key);
            return ResponseEntity.badRequest().build();
        }
        LOG.info("Starting update of Raffles.");
        return raffleInformationService.updateRaffleInformation()
                .map(raffleDTO -> ResponseEntity.ok(new RaffleResource(raffleDTO)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
