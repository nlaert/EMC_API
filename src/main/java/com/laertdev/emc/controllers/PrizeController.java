package com.laertdev.emc.controllers;

import com.laertdev.emc.DTO.resources.PrizeResource;
import com.laertdev.emc.services.RafflePrizesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController()
@RequestMapping("/prizes")
public class PrizeController {
    private RafflePrizesService prizesService;

    @Autowired
    public PrizeController(RafflePrizesService prizesService) {
        this.prizesService = prizesService;
    }

    public static Link getLink(int year, int number, boolean isSelf) {
        return linkTo(methodOn(PrizeController.class).prizeValue(year, number, null, null))
                .withRel(isSelf ? "self" : "prizes");
    }

    @GetMapping("/prizeValue")
    public ResponseEntity<PrizeResource> prizeValue(@RequestParam int year,
                                                    @RequestParam int raffleNumber,
                                                    @RequestParam String numbers,
                                                    @RequestParam String stars) {
        return prizesService.getPrizeValue(year, raffleNumber, numbers, stars)
                .map(prizeDTO -> ResponseEntity.ok(new PrizeResource(prizeDTO)))
                .orElse(ResponseEntity.notFound().build());
    }
}
