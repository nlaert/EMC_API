package com.laertdev.emc.controllers;

import com.laertdev.emc.DTO.resources.ResultResource;
import com.laertdev.emc.entities.Raffle;
import com.laertdev.emc.services.RaffleService;
import com.laertdev.emc.services.ResultsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController()
@RequestMapping("/results")
public class ResultsController {
    private static final Logger LOG = LoggerFactory.getLogger(ResultsController.class);
    private final ResultsService resultsService;
    private final RaffleService raffleService;

    @Autowired
    public ResultsController(ResultsService resultsService, RaffleService raffleService) {
        this.resultsService = resultsService;
        this.raffleService = raffleService;
    }

    public static Link getLink(int year, int number, boolean isSelf) {
        return linkTo(methodOn(ResultsController.class).getResultByYearAndNumber(year, number))
                .withRel(isSelf ? "self" : "results");
    }

    @GetMapping("/byYearAndNumber")
    public ResponseEntity<ResultResource> getResultByYearAndNumber(@RequestParam int year, @RequestParam int number) {
        return resultsService.getResultsByRaffleYearAndNumber(year, number)
                .map(resultDTO -> ResponseEntity.ok(new ResultResource(resultDTO, year, number)))
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/lastRaffle")
    public ResponseEntity<ResultResource> getLastRaffleResult() {
        Raffle lastRaffle = raffleService.getLastRaffle();
        return resultsService.getResultsByRaffleYearAndNumber(lastRaffle.getYear(), lastRaffle.getNumber())
                .map(resultDTO -> ResponseEntity.ok(new ResultResource(resultDTO,
                        lastRaffle.getYear(),
                        lastRaffle.getNumber())))
                .orElse(ResponseEntity.notFound().build());
    }
}
