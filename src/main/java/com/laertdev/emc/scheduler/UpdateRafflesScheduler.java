package com.laertdev.emc.scheduler;

import com.laertdev.emc.services.RaffleInformationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class UpdateRafflesScheduler {
    private static final Logger LOG = LoggerFactory.getLogger(UpdateRafflesScheduler.class);

    private final RaffleInformationService raffleInformationService;

    @Autowired
    public UpdateRafflesScheduler(RaffleInformationService raffleInformationService) {
        this.raffleInformationService = raffleInformationService;
    }

    @Scheduled(cron = "0 0/15 20-23 * * TUE,FRI")
    public void updateRaffles() {
        LOG.info("Starting scheduler to get today's raffle");
        raffleInformationService.updateRaffleInformation();
    }

}
