package com.laertdev.emc.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Raffle
{

    @Id
    @GeneratedValue
    private Long id;
    private int year;
    private int number;
    private java.sql.Date date;

    public Raffle()
    {
    }

    public Raffle(Long id, int year, int number, Date date)
    {
        this.id = id;
        this.year = year;
        this.number = number;
        this.date = date;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }

    public int getNumber()
    {
        return number;
    }

    public void setNumber(int number)
    {
        this.number = number;
    }

    public java.sql.Date getDate()
    {
        return date;
    }

    public LocalDate getLocalDate()
    {
        return date.toLocalDate();
    }

    public void setDate(java.sql.Date date) {
        this.date = date;
    }

    public void setDate(LocalDate localDate) {
        this.date = Date.valueOf(localDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Raffle raffle = (Raffle) o;
        return getYear() == raffle.getYear() &&
                getNumber() == raffle.getNumber() &&
                Objects.equals(getDate(), raffle.getDate());
    }
}
