package com.laertdev.emc.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity()
public class RafflePrizes
{

    @Id
    @GeneratedValue()
    private Long id;
    private Long raffleId;
    private int prizeOrder;
    private int amountOfNumbers;
    private int amountOfStars;
    private String prizeMoney;
    private int winners;

    public RafflePrizes()
    {
    }

    public RafflePrizes(Long id,
                        Long raffleId,
                        int prizeOrder,
                        int amountOfNumbers,
                        int amountOfStars,
                        String prizeMoney,
                        int winners)
    {
        this.id = id;
        this.raffleId = raffleId;
        this.prizeOrder = prizeOrder;
        this.amountOfNumbers = amountOfNumbers;
        this.amountOfStars = amountOfStars;
        this.prizeMoney = prizeMoney;
        this.winners = winners;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getRaffleId()
    {
        return raffleId;
    }

    public void setRaffleId(Long raffleId)
    {
        this.raffleId = raffleId;
    }

    public int getPrizeOrder()
    {
        return prizeOrder;
    }

    public void setPrizeOrder(int prizeOrder)
    {
        this.prizeOrder = prizeOrder;
    }

    public int getAmountOfNumbers()
    {
        return amountOfNumbers;
    }

    public void setAmountOfNumbers(int amountOfNumbers)
    {
        this.amountOfNumbers = amountOfNumbers;
    }

    public int getAmountOfStars()
    {
        return amountOfStars;
    }

    public void setAmountOfStars(int amountOfStars)
    {
        this.amountOfStars = amountOfStars;
    }

    public String getPrizeMoney()
    {
        return prizeMoney;
    }

    public void setPrizeMoney(String prizeMoney)
    {
        this.prizeMoney = prizeMoney;
    }

    public int getWinners()
    {
        return winners;
    }

    public void setWinners(int winners)
    {
        this.winners = winners;
    }
}
