package com.laertdev.emc.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class RaffleResult
{

    @Id
    @GeneratedValue
    private Long id;
    private Long raffleId;
    private String numbers;
    private String stars;

    public RaffleResult()
    {
    }

    public RaffleResult(Long id, Long raffleId, String numbers, String stars)
    {
        this.id = id;
        this.raffleId = raffleId;
        this.numbers = numbers;
        this.stars = stars;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getRaffleId()
    {
        return raffleId;
    }

    public void setRaffleId(Long raffleId)
    {
        this.raffleId = raffleId;
    }

    public String getNumbers()
    {
        return numbers;
    }

    public void setNumbers(String numbers)
    {
        this.numbers = numbers;
    }

    public String getStars()
    {
        return stars;
    }

    public void setStars(String stars)
    {
        this.stars = stars;
    }
}
