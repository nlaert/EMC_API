package com.laertdev.emc.DTO;

public class RaffleDTO {
    private int year;
    private int number;

    public RaffleDTO(int year, int number) {
        this.year = year;
        this.number = number;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
