package com.laertdev.emc.DTO.resources;

import com.laertdev.emc.DTO.RaffleDTO;
import com.laertdev.emc.controllers.PrizeController;
import com.laertdev.emc.controllers.RaffleController;
import com.laertdev.emc.controllers.ResultsController;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class RaffleResource extends ResourceSupport {
    private RaffleDTO raffleDTO;

    public RaffleResource(RaffleDTO raffleDTO) {
        this.raffleDTO = raffleDTO;
        int year = raffleDTO.getYear();
        int number = raffleDTO.getNumber();
        add(linkTo(methodOn(RaffleController.class).updateRaffle(null)).withSelfRel());
        add(PrizeController.getLink(year, number, false));
        add(ResultsController.getLink(year, number, false));
    }

    public RaffleDTO getRaffle() {
        return raffleDTO;
    }
}
