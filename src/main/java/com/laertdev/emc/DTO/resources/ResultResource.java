package com.laertdev.emc.DTO.resources;

import com.laertdev.emc.DTO.ResultDTO;
import com.laertdev.emc.controllers.PrizeController;
import com.laertdev.emc.controllers.ResultsController;
import org.springframework.hateoas.ResourceSupport;

public class ResultResource extends ResourceSupport {
    private ResultDTO resultDTO;

    public ResultResource(ResultDTO resultDTO, int raffleYear, int raffleNumber) {
        this.resultDTO = resultDTO;
        add(ResultsController.getLink(raffleYear, raffleNumber, true));
        add(PrizeController.getLink(raffleYear, raffleNumber, false));
    }

    public ResultDTO getResult() {
        return resultDTO;
    }
}
