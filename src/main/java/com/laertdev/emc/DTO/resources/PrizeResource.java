package com.laertdev.emc.DTO.resources;

import com.laertdev.emc.DTO.PrizeDTO;
import com.laertdev.emc.controllers.PrizeController;
import com.laertdev.emc.controllers.ResultsController;
import org.springframework.hateoas.ResourceSupport;

public class PrizeResource extends ResourceSupport {
    private PrizeDTO prizeDTO;

    public PrizeResource(PrizeDTO prizeDTO) {
        this.prizeDTO = prizeDTO;
        int raffleYear = prizeDTO.getYear();
        int raffleNumber = prizeDTO.getRaffleNumber();
        add(PrizeController.getLink(raffleYear, raffleNumber, true));
        add(ResultsController.getLink(raffleYear, raffleNumber, false));
    }

    public PrizeDTO getPrizes() {
        return prizeDTO;
    }
}
