package com.laertdev.emc.DTO;

import java.util.Arrays;

public class ResultDTO
{
    private String[] numbers;
    private String[] stars;

    public ResultDTO(String[] numbers, String[] stars)
    {
        this.numbers = numbers;
        this.stars = stars;
    }

    public String[] getNumbers()
    {
        return numbers;
    }

    public void setNumbers(String[] numbers)
    {
        this.numbers = numbers;
    }

    public String[] getStars()
    {
        return stars;
    }

    public void setStars(String[] stars)
    {
        this.stars = stars;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        ResultDTO resultDTO = (ResultDTO) o;
        return Arrays.equals(numbers, resultDTO.numbers) &&
                Arrays.equals(stars, resultDTO.stars);
    }

    @Override
    public int hashCode()
    {
        int result = Arrays.hashCode(numbers);
        result = 31 * result + Arrays.hashCode(stars);
        return result;
    }
}
