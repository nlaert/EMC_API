package com.laertdev.emc.DTO;

import java.time.LocalDate;
import java.util.Objects;

public class PrizeDTO
{
    private int year;
    private int raffleNumber;
    private LocalDate raffleDate;
    private int prizeOrder;
    private String numbersHit;
    private String starsHit;
    private String prizeMoney;
    private int winners;

    public PrizeDTO(int year,
                    int raffleNumber,
                    LocalDate raffleDate,
                    int prizeOrder,
                    String numbersHit,
                    String starsHit,
                    String prizeMoney,
                    int winners) {
        this.year = year;
        this.raffleNumber = raffleNumber;
        this.raffleDate = raffleDate;
        this.prizeOrder = prizeOrder;
        this.numbersHit = numbersHit;
        this.starsHit = starsHit;
        this.prizeMoney = prizeMoney;
        this.winners = winners;
    }

    public PrizeDTO(int year, int raffleNumber, LocalDate raffleDate) {
        this(year, raffleNumber, raffleDate, 0, "", "", "", 0);
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getRaffleNumber()
    {
        return raffleNumber;
    }

    public void setRaffleNumber(int raffleNumber)
    {
        this.raffleNumber = raffleNumber;
    }

    public LocalDate getRaffleDate()
    {
        return raffleDate;
    }

    public void setRaffleDate(LocalDate raffleDate)
    {
        this.raffleDate = raffleDate;
    }

    public int getPrizeOrder()
    {
        return prizeOrder;
    }

    public void setPrizeOrder(int prizeOrder)
    {
        this.prizeOrder = prizeOrder;
    }

    public String getNumbersHit()
    {
        return numbersHit;
    }

    public void setNumbersHit(String numbersHit)
    {
        this.numbersHit = numbersHit;
    }

    public String getStarsHit()
    {
        return starsHit;
    }

    public void setStarsHit(String starsHit)
    {
        this.starsHit = starsHit;
    }

    public String getPrizeMoney()
    {
        return prizeMoney;
    }

    public void setPrizeMoney(String prizeMoney)
    {
        this.prizeMoney = prizeMoney;
    }

    public int getWinners()
    {
        return winners;
    }

    public void setWinners(int winners)
    {
        this.winners = winners;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        PrizeDTO prizeDTO = (PrizeDTO) o;
        return year == prizeDTO.year &&
                raffleNumber == prizeDTO.raffleNumber &&
                prizeOrder == prizeDTO.prizeOrder &&
                winners == prizeDTO.winners &&
                Objects.equals(raffleDate, prizeDTO.raffleDate) &&
                Objects.equals(numbersHit, prizeDTO.numbersHit) &&
                Objects.equals(starsHit, prizeDTO.starsHit) &&
                Objects.equals(prizeMoney, prizeDTO.prizeMoney);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(year, raffleNumber, raffleDate, prizeOrder, numbersHit, starsHit, prizeMoney, winners);
    }
}
