package com.laertdev.emc.services;

import com.laertdev.emc.DTO.ResultDTO;

import java.util.Optional;

public interface ResultsService {
    Optional<ResultDTO> getResultsByRaffleYearAndNumber(int raffleYear, int raffleNumber);
}
