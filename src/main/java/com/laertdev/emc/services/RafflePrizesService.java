package com.laertdev.emc.services;

import com.laertdev.emc.DTO.PrizeDTO;

import java.util.Optional;

public interface RafflePrizesService
{
    Optional<PrizeDTO> getPrizeValue(int year, int raffleNumber, String numbers, String stars);
}
