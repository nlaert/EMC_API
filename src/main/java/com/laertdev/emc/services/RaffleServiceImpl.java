package com.laertdev.emc.services;

import com.laertdev.emc.entities.Raffle;
import com.laertdev.emc.repositories.RaffleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class RaffleServiceImpl implements RaffleService {
    private RaffleRepository raffleRepository;

    @Autowired
    public RaffleServiceImpl(RaffleRepository raffleRepository) {
        this.raffleRepository = raffleRepository;
    }

    @Override
    public Raffle getLastRaffle() {
        Raffle lastRaffle = raffleRepository.findFirstByYearOrderByDateDesc(LocalDate.now().getYear());

        // Let's just make sure we're not in the beginning of the year and there was no raffle this year yet.
        if (lastRaffle == null) {
            lastRaffle = raffleRepository.findFirstByYearOrderByDateDesc(LocalDate.now().minusYears(1).getYear());
        }

        return lastRaffle;
    }
}
