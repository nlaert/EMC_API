package com.laertdev.emc.services;

import com.laertdev.emc.DTO.PrizeDTO;
import com.laertdev.emc.entities.Raffle;
import com.laertdev.emc.entities.RafflePrizes;
import com.laertdev.emc.entities.RaffleResult;
import com.laertdev.emc.repositories.RafflePrizesRepository;
import com.laertdev.emc.repositories.RaffleResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.laertdev.emc.EmcApplication.VALUE_SEPARATOR;

@Service
public class RafflePrizesServiceImpl implements RafflePrizesService {
    private static final Logger LOG = LoggerFactory.getLogger(RafflePrizesServiceImpl.class);
    private RafflePrizesRepository prizesRepository;
    private RaffleResultRepository resultRepository;

    @Autowired
    public RafflePrizesServiceImpl(RafflePrizesRepository prizesRepository,
                                   RaffleResultRepository resultRepository) {
        this.prizesRepository = prizesRepository;
        this.resultRepository = resultRepository;
    }

    @Override
    public Optional<PrizeDTO> getPrizeValue(int year, int raffleNumber, String numbers, String stars) {
        Tuple tuple = resultRepository.getByYearAndNumberJoinRaffle(year, raffleNumber);
        if (tuple == null) {
            LOG.info("Could not find Raffle for year {} and number {}", year, raffleNumber);
            return Optional.empty();
        }
        RaffleResult result = (RaffleResult) tuple.get(0);
        Raffle raffle = (Raffle) tuple.get(1);

        List<String> resultNumbers = Arrays.asList(result.getNumbers().split(VALUE_SEPARATOR));
        List<String> resultStars = Arrays.asList(result.getStars().split(VALUE_SEPARATOR));

        List<String> numbersHit = Arrays.stream(numbers.split(VALUE_SEPARATOR))
                .filter(resultNumbers::contains)
                .collect(Collectors.toList());
        List<String> starsHit = Arrays.stream(stars.split(VALUE_SEPARATOR))
                .filter(resultStars::contains)
                .collect(Collectors.toList());

        RafflePrizes prizes = prizesRepository.getByRaffleIdAndAmountOfNumbersAndAmountOfStars(
                result.getRaffleId(),
                numbersHit.size(),
                starsHit.size());

        // No prize
        if (prizes == null) {
            return Optional.of(new PrizeDTO(year, raffleNumber, raffle.getLocalDate()));
        }

        return Optional.of(new PrizeDTO(year,
                raffleNumber,
                raffle.getLocalDate(),
                prizes.getPrizeOrder(),
                String.join(VALUE_SEPARATOR, numbersHit),
                String.join(VALUE_SEPARATOR, starsHit),
                prizes.getPrizeMoney(),
                prizes.getWinners()));
    }
}
