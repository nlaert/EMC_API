package com.laertdev.emc.services;

import com.laertdev.emc.DTO.RaffleDTO;

import java.util.Optional;

public interface RaffleInformationService {

    Optional<RaffleDTO> updateRaffleInformation();
}
