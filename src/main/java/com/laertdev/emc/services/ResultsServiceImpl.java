package com.laertdev.emc.services;

import com.laertdev.emc.DTO.ResultDTO;
import com.laertdev.emc.entities.RaffleResult;
import com.laertdev.emc.repositories.RaffleResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.laertdev.emc.EmcApplication.VALUE_SEPARATOR;

@Service
public class ResultsServiceImpl implements ResultsService {

    private RaffleResultRepository resultRepository;

    @Autowired
    public ResultsServiceImpl(RaffleResultRepository resultRepository) {
        this.resultRepository = resultRepository;
    }

    @Override
    public Optional<ResultDTO> getResultsByRaffleYearAndNumber(int raffleYear, int raffleNumber) {
        RaffleResult result = resultRepository.getByYearAndNumber(raffleYear, raffleNumber);

        if (result == null) {
            return Optional.empty();
        }

        return Optional.of(new ResultDTO(result.getNumbers().split(VALUE_SEPARATOR),
                result.getStars().split(VALUE_SEPARATOR)));
    }
}
