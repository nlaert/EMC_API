package com.laertdev.emc.services;

import com.laertdev.emc.entities.Raffle;

public interface RaffleService {
    Raffle getLastRaffle();
}
