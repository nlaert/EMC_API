package com.laertdev.emc.services;

import com.laertdev.emc.DTO.RaffleDTO;
import com.laertdev.emc.entities.Raffle;
import com.laertdev.emc.entities.RafflePrizes;
import com.laertdev.emc.entities.RaffleResult;
import com.laertdev.emc.external.ExternalAPIService;
import com.laertdev.emc.external.RaffleInformation;
import com.laertdev.emc.external.RaffleNotProcessedException;
import com.laertdev.emc.repositories.RafflePrizesRepository;
import com.laertdev.emc.repositories.RaffleRepository;
import com.laertdev.emc.repositories.RaffleResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class RaffleInformationServiceImpl implements RaffleInformationService {

    private static final Logger LOG = LoggerFactory.getLogger(RaffleInformationServiceImpl.class);

    private RaffleRepository raffleRepository;
    private RaffleResultRepository raffleResultRepository;
    private RafflePrizesRepository rafflePrizesRepository;
    private ExternalAPIService externalAPIService;

    @Autowired
    public RaffleInformationServiceImpl(RaffleRepository raffleRepository,
                                        RaffleResultRepository raffleResultRepository,
                                        RafflePrizesRepository rafflePrizesRepository,
                                        ExternalAPIService externalAPIService) {
        this.raffleRepository = raffleRepository;
        this.raffleResultRepository = raffleResultRepository;
        this.rafflePrizesRepository = rafflePrizesRepository;
        this.externalAPIService = externalAPIService;
    }

    @Override
    public Optional<RaffleDTO> updateRaffleInformation() {
        LocalDate currentDate = LocalDate.now();
        Raffle lastRaffle = raffleRepository.findFirstByYearOrderByDateDesc(currentDate.getYear());

        if (lastRaffle != null && currentDate.isEqual(lastRaffle.getLocalDate())) {
            LOG.info("Already got today's raffle.");
            return Optional.of(new RaffleDTO(lastRaffle.getYear(), lastRaffle.getNumber()));
        }

        RaffleInformation raffleInformation;
        try {
            raffleInformation = externalAPIService.getLastDrawResult();
        } catch (RaffleNotProcessedException e) {
            return Optional.empty();
        }

        Raffle raffle = raffleInformation.getRaffle();

        if (raffle.equals(lastRaffle)) {
            LOG.info("Already got today's raffle.");
            return Optional.of(new RaffleDTO(lastRaffle.getYear(), lastRaffle.getNumber()));
        }
        lastRaffle = raffleRepository.save(raffle);

        raffleInformation.setRaffle(lastRaffle);
        RaffleResult raffleResult = raffleInformation.getRaffleResult();
        raffleResult.setRaffleId(lastRaffle.getId());
        raffleResultRepository.save(raffleResult);

        for (RafflePrizes prizes : raffleInformation.getRafflePrizes()) {
            prizes.setRaffleId(lastRaffle.getId());
            rafflePrizesRepository.save(prizes);
        }

        LOG.info("Finished saving information for today's raffle. year {}, raffleNumber {}",
                raffle.getYear(),
                raffle.getNumber());

        return Optional.of(new RaffleDTO(lastRaffle.getYear(), lastRaffle.getNumber()));
    }
}

