package com.laertdev.emc.external;

public class RaffleNotProcessedException extends Exception {
    private String state;

    public RaffleNotProcessedException(String state)
    {
        super("Raffle not processed yet");
        this.state = state;
    }

    public RaffleNotProcessedException(String errorMessage, String state)
    {
        super(errorMessage);
        this.state = state;
    }

    public String getState()
    {
        return state;
    }
}
