package com.laertdev.emc.external;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class Utils {
    public static int calculateRaffleNumber(LocalDate raffleDate) {
        LocalDate firstDayOfYear = LocalDate.of(raffleDate.getYear(), 1, 1);
        DayOfWeek firstDayOfYearDayOfWeek = firstDayOfYear.getDayOfWeek();
        boolean yearStartsOnTuesday = (firstDayOfYearDayOfWeek.equals(DayOfWeek.SUNDAY) ||
                firstDayOfYearDayOfWeek.equals(DayOfWeek.MONDAY) ||
                firstDayOfYearDayOfWeek.equals(DayOfWeek.TUESDAY));

        DayOfWeek raffleDayOfWeek = raffleDate.getDayOfWeek();
        boolean isTuesdayRaffle = (raffleDayOfWeek.equals(DayOfWeek.TUESDAY) ||
                raffleDayOfWeek.equals(DayOfWeek.WEDNESDAY) ||
                raffleDayOfWeek.equals(DayOfWeek.THURSDAY));

        TemporalField woy = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();
        int raffleNumber = raffleDate.get(woy) * 2;

        if (raffleNumber < 3 && raffleDate.getMonthValue() == 12) {
            raffleNumber = lastWeekOfYear(raffleDate);
        }

        if (yearStartsOnTuesday) {
            return isTuesdayRaffle ? raffleNumber - 1 : raffleNumber;
        }
        return isTuesdayRaffle ? raffleNumber - 2 : raffleNumber - 1;
    }

    /**
     * Weird hack for when the year ends at the middle of the week, Java says that last week of December is week 1.
     *
     * @param raffleDate the raffle date
     * @return the calculated raffle number
     */
    private static int lastWeekOfYear(LocalDate raffleDate) {
        int numberOfDaysToGoBack = 4;
        if (raffleDate.getDayOfWeek().equals(DayOfWeek.FRIDAY)) {
            numberOfDaysToGoBack = 8;
        }

        LocalDate tempDate = LocalDate.of(raffleDate.getYear(),
                raffleDate.getMonth(),
                raffleDate.getDayOfMonth() - numberOfDaysToGoBack);

        return calculateRaffleNumber(tempDate) + 2;
    }
}
