
package com.laertdev.emc.external.eLotto.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Game
{

    @JsonProperty("iddggame")
    private Double iddggame;
    @JsonProperty("iddggametype")
    private String iddggametype;
    @JsonProperty("externalId")
    private String externalId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("state")
    private String state;
    @JsonProperty("issubscriptionsallowed")
    private Double issubscriptionsallowed;
    @JsonProperty("openTime")
    private String openTime;
    @JsonProperty("closeTime")
    private String closeTime;
    @JsonProperty("revision")
    private Double revision;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("iddggame")
    public Double getIddggame()
    {
        return iddggame;
    }

    @JsonProperty("iddggame")
    public void setIddggame(Double iddggame)
    {
        this.iddggame = iddggame;
    }

    @JsonProperty("iddggametype")
    public String getIddggametype()
    {
        return iddggametype;
    }

    @JsonProperty("iddggametype")
    public void setIddggametype(String iddggametype)
    {
        this.iddggametype = iddggametype;
    }

    @JsonProperty("externalId")
    public String getExternalId()
    {
        return externalId;
    }

    @JsonProperty("externalId")
    public void setExternalId(String externalId)
    {
        this.externalId = externalId;
    }

    @JsonProperty("name")
    public String getName()
    {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name)
    {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription()
    {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description)
    {
        this.description = description;
    }

    @JsonProperty("state")
    public String getState()
    {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state)
    {
        this.state = state;
    }

    @JsonProperty("issubscriptionsallowed")
    public Double getIssubscriptionsallowed()
    {
        return issubscriptionsallowed;
    }

    @JsonProperty("issubscriptionsallowed")
    public void setIssubscriptionsallowed(Double issubscriptionsallowed)
    {
        this.issubscriptionsallowed = issubscriptionsallowed;
    }

    @JsonProperty("openTime")
    public String getOpenTime()
    {
        return openTime;
    }

    @JsonProperty("openTime")
    public void setOpenTime(String openTime)
    {
        this.openTime = openTime;
    }

    @JsonProperty("closeTime")
    public String getCloseTime()
    {
        return closeTime;
    }

    @JsonProperty("closeTime")
    public void setCloseTime(String closeTime)
    {
        this.closeTime = closeTime;
    }

    @JsonProperty("revision")
    public Double getRevision()
    {
        return revision;
    }

    @JsonProperty("revision")
    public void setRevision(Double revision)
    {
        this.revision = revision;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

}
