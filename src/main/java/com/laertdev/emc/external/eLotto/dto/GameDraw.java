package com.laertdev.emc.external.eLotto.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GameDraw
{
    @JsonProperty("iddggamedraw")
    private Double iddggamedraw;
    @JsonProperty("iddggameevent")
    private Double iddggameevent;
    @JsonProperty("name")
    private String name;
    @JsonProperty("rolloveramount")
    private Object rolloveramount;
    @JsonProperty("isrolldownpossible")
    private Object isrolldownpossible;
    @JsonProperty("actualjackpot")
    private Double actualjackpot;
    @JsonProperty("estimatedjackpot")
    private Object estimatedjackpot;
    @JsonProperty("guaranteedjackpot")
    private Object guaranteedjackpot;
    @JsonProperty("boostedjackpot")
    private Object boostedjackpot;
    @JsonProperty("totalprizeamount")
    private Object totalprizeamount;
    @JsonProperty("totalguaranteedjackpot")
    private Object totalguaranteedjackpot;
    @JsonProperty("jackpotwinners")
    private Double jackpotwinners;
    @JsonProperty("quickpickwinners")
    private Object quickpickwinners;
    @JsonProperty("tsresult")
    private String tsresult;
    @JsonProperty("jackpotdivisionlevelid")
    private Double jackpotdivisionlevelid;
    @JsonProperty("resultSets")
    private List<ResultSet> resultSets = null;
    @JsonProperty("resultDivisions")
    private List<ResultDivision> resultDivisions = null;
    @JsonProperty("drawid")
    private Double drawid;
    @JsonProperty("topWinAmount")
    private Double topWinAmount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("iddggamedraw")
    public Double getIddggamedraw()
    {
        return iddggamedraw;
    }

    @JsonProperty("iddggamedraw")
    public void setIddggamedraw(Double iddggamedraw)
    {
        this.iddggamedraw = iddggamedraw;
    }

    @JsonProperty("iddggameevent")
    public Double getIddggameevent()
    {
        return iddggameevent;
    }

    @JsonProperty("iddggameevent")
    public void setIddggameevent(Double iddggameevent)
    {
        this.iddggameevent = iddggameevent;
    }

    @JsonProperty("name")
    public String getName()
    {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name)
    {
        this.name = name;
    }

    @JsonProperty("rolloveramount")
    public Object getRolloveramount()
    {
        return rolloveramount;
    }

    @JsonProperty("rolloveramount")
    public void setRolloveramount(Object rolloveramount)
    {
        this.rolloveramount = rolloveramount;
    }

    @JsonProperty("isrolldownpossible")
    public Object getIsrolldownpossible()
    {
        return isrolldownpossible;
    }

    @JsonProperty("isrolldownpossible")
    public void setIsrolldownpossible(Object isrolldownpossible)
    {
        this.isrolldownpossible = isrolldownpossible;
    }

    @JsonProperty("actualjackpot")
    public Double getActualjackpot()
    {
        return actualjackpot;
    }

    @JsonProperty("actualjackpot")
    public void setActualjackpot(Double actualjackpot)
    {
        this.actualjackpot = actualjackpot;
    }

    @JsonProperty("estimatedjackpot")
    public Object getEstimatedjackpot()
    {
        return estimatedjackpot;
    }

    @JsonProperty("estimatedjackpot")
    public void setEstimatedjackpot(Object estimatedjackpot)
    {
        this.estimatedjackpot = estimatedjackpot;
    }

    @JsonProperty("guaranteedjackpot")
    public Object getGuaranteedjackpot()
    {
        return guaranteedjackpot;
    }

    @JsonProperty("guaranteedjackpot")
    public void setGuaranteedjackpot(Object guaranteedjackpot)
    {
        this.guaranteedjackpot = guaranteedjackpot;
    }

    @JsonProperty("boostedjackpot")
    public Object getBoostedjackpot()
    {
        return boostedjackpot;
    }

    @JsonProperty("boostedjackpot")
    public void setBoostedjackpot(Object boostedjackpot)
    {
        this.boostedjackpot = boostedjackpot;
    }

    @JsonProperty("totalprizeamount")
    public Object getTotalprizeamount()
    {
        return totalprizeamount;
    }

    @JsonProperty("totalprizeamount")
    public void setTotalprizeamount(Object totalprizeamount)
    {
        this.totalprizeamount = totalprizeamount;
    }

    @JsonProperty("totalguaranteedjackpot")
    public Object getTotalguaranteedjackpot()
    {
        return totalguaranteedjackpot;
    }

    @JsonProperty("totalguaranteedjackpot")
    public void setTotalguaranteedjackpot(Object totalguaranteedjackpot)
    {
        this.totalguaranteedjackpot = totalguaranteedjackpot;
    }

    @JsonProperty("jackpotwinners")
    public Double getJackpotwinners()
    {
        return jackpotwinners;
    }

    @JsonProperty("jackpotwinners")
    public void setJackpotwinners(Double jackpotwinners)
    {
        this.jackpotwinners = jackpotwinners;
    }

    @JsonProperty("quickpickwinners")
    public Object getQuickpickwinners()
    {
        return quickpickwinners;
    }

    @JsonProperty("quickpickwinners")
    public void setQuickpickwinners(Object quickpickwinners)
    {
        this.quickpickwinners = quickpickwinners;
    }

    @JsonProperty("tsresult")
    public String getTsresult()
    {
        return tsresult;
    }

    @JsonProperty("tsresult")
    public void setTsresult(String tsresult)
    {
        this.tsresult = tsresult;
    }

    @JsonProperty("jackpotdivisionlevelid")
    public Double getJackpotdivisionlevelid()
    {
        return jackpotdivisionlevelid;
    }

    @JsonProperty("jackpotdivisionlevelid")
    public void setJackpotdivisionlevelid(Double jackpotdivisionlevelid)
    {
        this.jackpotdivisionlevelid = jackpotdivisionlevelid;
    }

    @JsonProperty("resultSets")
    public List<ResultSet> getResultSets()
    {
        return resultSets;
    }

    @JsonProperty("resultSets")
    public void setResultSets(List<ResultSet> resultSets)
    {
        this.resultSets = resultSets;
    }

    @JsonProperty("resultDivisions")
    public List<ResultDivision> getResultDivisions()
    {
        return resultDivisions;
    }

    @JsonProperty("resultDivisions")
    public void setResultDivisions(List<ResultDivision> resultDivisions)
    {
        this.resultDivisions = resultDivisions;
    }

    @JsonProperty("drawid")
    public Double getDrawid()
    {
        return drawid;
    }

    @JsonProperty("drawid")
    public void setDrawid(Double drawid)
    {
        this.drawid = drawid;
    }

    @JsonProperty("topWinAmount")
    public Double getTopWinAmount()
    {
        return topWinAmount;
    }

    @JsonProperty("topWinAmount")
    public void setTopWinAmount(Double topWinAmount)
    {
        this.topWinAmount = topWinAmount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

}
