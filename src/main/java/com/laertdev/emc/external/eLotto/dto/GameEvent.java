
package com.laertdev.emc.external.eLotto.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GameEvent
{

    @JsonProperty("game")
    private Game game;
    @JsonProperty("iddggameevent")
    private Double iddggameevent;
    @JsonProperty("iddggame")
    private Double iddggame;
    @JsonProperty("drawnumber")
    private Double drawnumber;
    @JsonProperty("drawdate")
    private String drawdate;
    @JsonProperty("seriesid")
    private String seriesid;
    @JsonProperty("name")
    private String name;
    @JsonProperty("state")
    private String state;
    @JsonProperty("iddggamedraw_main")
    private Double iddggamedrawMain;
    @JsonProperty("salesopentime")
    private String salesopentime;
    @JsonProperty("wagerclosetime")
    private String wagerclosetime;
    @JsonProperty("drawclosetime")
    private String drawclosetime;
    @JsonProperty("cancelclosetime")
    private String cancelclosetime;
    @JsonProperty("resulttime")
    private String resulttime;
    @JsonProperty("isopenforwager")
    private Double isopenforwager;
    @JsonProperty("isopenforcancel")
    private Double isopenforcancel;
    @JsonProperty("areresultsetsvisible")
    private Double areresultsetsvisible;
    @JsonProperty("areresultdivisionsvisible")
    private Double areresultdivisionsvisible;
    @JsonProperty("previousdrawdate")
    private String previousdrawdate;
    @JsonProperty("previousdrawclosetime")
    private String previousdrawclosetime;
    @JsonProperty("nextdrawdate")
    private Object nextdrawdate;
    @JsonProperty("nextdrawclosetime")
    private Object nextdrawclosetime;
    @JsonProperty("gamedraws")
    private List<GameDraw> gameDraws = null;
    @JsonProperty("maindraw")
    private MainDraw mainDraw;
    @JsonProperty("params")
    private Object params;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("game")
    public Game getGame()
    {
        return game;
    }

    @JsonProperty("game")
    public void setGame(Game game)
    {
        this.game = game;
    }

    @JsonProperty("iddggameevent")
    public Double getIddggameevent()
    {
        return iddggameevent;
    }

    @JsonProperty("iddggameevent")
    public void setIddggameevent(Double iddggameevent)
    {
        this.iddggameevent = iddggameevent;
    }

    @JsonProperty("iddggame")
    public Double getIddggame()
    {
        return iddggame;
    }

    @JsonProperty("iddggame")
    public void setIddggame(Double iddggame)
    {
        this.iddggame = iddggame;
    }

    @JsonProperty("drawnumber")
    public Double getDrawnumber()
    {
        return drawnumber;
    }

    @JsonProperty("drawnumber")
    public void setDrawnumber(Double drawnumber)
    {
        this.drawnumber = drawnumber;
    }

    @JsonProperty("drawdate")
    public String getDrawdate()
    {
        return drawdate;
    }

    @JsonProperty("drawdate")
    public void setDrawdate(String drawdate)
    {
        this.drawdate = drawdate;
    }

    @JsonProperty("seriesid")
    public String getSeriesid()
    {
        return seriesid;
    }

    @JsonProperty("seriesid")
    public void setSeriesid(String seriesid)
    {
        this.seriesid = seriesid;
    }

    @JsonProperty("name")
    public String getName()
    {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name)
    {
        this.name = name;
    }

    @JsonProperty("state")
    public String getState()
    {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state)
    {
        this.state = state;
    }

    @JsonProperty("iddggamedraw_main")
    public Double getIddggamedrawMain()
    {
        return iddggamedrawMain;
    }

    @JsonProperty("iddggamedraw_main")
    public void setIddggamedrawMain(Double iddggamedrawMain)
    {
        this.iddggamedrawMain = iddggamedrawMain;
    }

    @JsonProperty("salesopentime")
    public String getSalesopentime()
    {
        return salesopentime;
    }

    @JsonProperty("salesopentime")
    public void setSalesopentime(String salesopentime)
    {
        this.salesopentime = salesopentime;
    }

    @JsonProperty("wagerclosetime")
    public String getWagerclosetime()
    {
        return wagerclosetime;
    }

    @JsonProperty("wagerclosetime")
    public void setWagerclosetime(String wagerclosetime)
    {
        this.wagerclosetime = wagerclosetime;
    }

    @JsonProperty("drawclosetime")
    public String getDrawclosetime()
    {
        return drawclosetime;
    }

    @JsonProperty("drawclosetime")
    public void setDrawclosetime(String drawclosetime)
    {
        this.drawclosetime = drawclosetime;
    }

    @JsonProperty("cancelclosetime")
    public String getCancelclosetime()
    {
        return cancelclosetime;
    }

    @JsonProperty("cancelclosetime")
    public void setCancelclosetime(String cancelclosetime)
    {
        this.cancelclosetime = cancelclosetime;
    }

    @JsonProperty("resulttime")
    public String getResulttime()
    {
        return resulttime;
    }

    @JsonProperty("resulttime")
    public void setResulttime(String resulttime)
    {
        this.resulttime = resulttime;
    }

    @JsonProperty("isopenforwager")
    public Double getIsopenforwager()
    {
        return isopenforwager;
    }

    @JsonProperty("isopenforwager")
    public void setIsopenforwager(Double isopenforwager)
    {
        this.isopenforwager = isopenforwager;
    }

    @JsonProperty("isopenforcancel")
    public Double getIsopenforcancel()
    {
        return isopenforcancel;
    }

    @JsonProperty("isopenforcancel")
    public void setIsopenforcancel(Double isopenforcancel)
    {
        this.isopenforcancel = isopenforcancel;
    }

    @JsonProperty("areresultsetsvisible")
    public Double getAreresultsetsvisible()
    {
        return areresultsetsvisible;
    }

    @JsonProperty("areresultsetsvisible")
    public void setAreresultsetsvisible(Double areresultsetsvisible)
    {
        this.areresultsetsvisible = areresultsetsvisible;
    }

    @JsonProperty("areresultdivisionsvisible")
    public Double getAreresultdivisionsvisible()
    {
        return areresultdivisionsvisible;
    }

    @JsonProperty("areresultdivisionsvisible")
    public void setAreresultdivisionsvisible(Double areresultdivisionsvisible)
    {
        this.areresultdivisionsvisible = areresultdivisionsvisible;
    }

    @JsonProperty("previousdrawdate")
    public String getPreviousdrawdate()
    {
        return previousdrawdate;
    }

    @JsonProperty("previousdrawdate")
    public void setPreviousdrawdate(String previousdrawdate)
    {
        this.previousdrawdate = previousdrawdate;
    }

    @JsonProperty("previousdrawclosetime")
    public String getPreviousdrawclosetime()
    {
        return previousdrawclosetime;
    }

    @JsonProperty("previousdrawclosetime")
    public void setPreviousdrawclosetime(String previousdrawclosetime)
    {
        this.previousdrawclosetime = previousdrawclosetime;
    }

    @JsonProperty("nextdrawdate")
    public Object getNextdrawdate()
    {
        return nextdrawdate;
    }

    @JsonProperty("nextdrawdate")
    public void setNextdrawdate(Object nextdrawdate)
    {
        this.nextdrawdate = nextdrawdate;
    }

    @JsonProperty("nextdrawclosetime")
    public Object getNextdrawclosetime()
    {
        return nextdrawclosetime;
    }

    @JsonProperty("nextdrawclosetime")
    public void setNextdrawclosetime(Object nextdrawclosetime)
    {
        this.nextdrawclosetime = nextdrawclosetime;
    }

    @JsonProperty("gamedraws")
    public List<GameDraw> getGameDraws()
    {
        return gameDraws;
    }

    @JsonProperty("gamedraws")
    public void setGameDraws(List<GameDraw> gameDraws)
    {
        this.gameDraws = gameDraws;
    }

    @JsonProperty("maindraw")
    public MainDraw getMainDraw()
    {
        return mainDraw;
    }

    @JsonProperty("maindraw")
    public void setMainDraw(MainDraw mainDraw)
    {
        this.mainDraw = mainDraw;
    }

    @JsonProperty("params")
    public Object getParams()
    {
        return params;
    }

    @JsonProperty("params")
    public void setParams(Object params)
    {
        this.params = params;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

}
