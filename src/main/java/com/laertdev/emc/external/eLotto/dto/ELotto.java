
package com.laertdev.emc.external.eLotto.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ELotto
{

    @JsonProperty("gameevent")
    private GameEvent gameEvent;
    @JsonProperty("addonresults")
    private List<AddonResult> addonResults = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("gameevent")
    public GameEvent getGameEvent()
    {
        return gameEvent;
    }

    @JsonProperty("gameevent")
    public void setGameEvent(GameEvent gameEvent)
    {
        this.gameEvent = gameEvent;
    }

    @JsonProperty("addonresults")
    public List<AddonResult> getAddonResults()
    {
        return addonResults;
    }

    @JsonProperty("addonresults")
    public void setAddonResults(List<AddonResult> addonResults)
    {
        this.addonResults = addonResults;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

}
