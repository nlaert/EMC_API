package com.laertdev.emc.external.eLotto;

import com.laertdev.emc.external.ExternalAPIService;
import com.laertdev.emc.external.RaffleInformation;
import com.laertdev.emc.external.RaffleNotProcessedException;
import com.laertdev.emc.external.eLotto.dto.ELotto;
import com.laertdev.emc.external.eLotto.dto.GameEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service("externalAPIService")
public class eLottoService implements ExternalAPIService {

    private final RestTemplateBuilder restTemplateBuilder;

    private static final String PROCESSED = "PROCESSED";
    private static final Logger LOG = LoggerFactory.getLogger(eLottoService.class);
    @Value("${externalAPI.e-lotto.url:https://www.e-lotto.be/cache/dgLastResultForGameWithAddons/EN/}")
    private String eLottoURL;
    @Value("${externalAPI.e-lotto.file.name:Elot}")
    private String URLFileName;
    @Value("${externalAPI.e-lotto.file.extension:.json}")
    private String URLFileExtension;

    @Autowired
    public eLottoService(RestTemplateBuilder restTemplateBuilder)
    {
        this.restTemplateBuilder = restTemplateBuilder;
    }

    @Override
    public RaffleInformation getLastDrawResult() throws RaffleNotProcessedException {
        RestTemplate restTemplate = restTemplateBuilder.build();
        ELotto eLotto = restTemplate.getForObject(eLottoURL + URLFileName + URLFileExtension, ELotto.class);
        assert eLotto != null;
        GameEvent gameEvent = eLotto.getGameEvent();
        if (PROCESSED.equals(gameEvent.getState())) {
            return ELottoTransformer.transform(gameEvent);
        }

        LOG.warn("Euromillions draw is not processed yet.");

        throw new RaffleNotProcessedException(gameEvent.getState());
    }
}
