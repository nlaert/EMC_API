
package com.laertdev.emc.external.eLotto.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultSet
{

    @JsonProperty("setid")
    private Double setid;
    @JsonProperty("mainvalues")
    private String mainvalues;
    @JsonProperty("bonusvalues")
    private Object bonusvalues;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("setid")
    public Double getSetid()
    {
        return setid;
    }

    @JsonProperty("setid")
    public void setSetid(Double setid)
    {
        this.setid = setid;
    }

    @JsonProperty("mainvalues")
    public String getMainvalues()
    {
        return mainvalues;
    }

    @JsonProperty("mainvalues")
    public void setMainvalues(String mainvalues)
    {
        this.mainvalues = mainvalues;
    }

    @JsonProperty("bonusvalues")
    public Object getBonusvalues()
    {
        return bonusvalues;
    }

    @JsonProperty("bonusvalues")
    public void setBonusvalues(Object bonusvalues)
    {
        this.bonusvalues = bonusvalues;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

}
