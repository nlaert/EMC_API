
package com.laertdev.emc.external.eLotto.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MainDraw
{

    @JsonProperty("iddggamedraw")
    private Double iddggamedraw;
    @JsonProperty("iddggameevent")
    private Double iddggameevent;
    @JsonProperty("name")
    private String name;
    @JsonProperty("rolloveramount")
    private Double rolloveramount;
    @JsonProperty("isrolldownpossible")
    private Double isrolldownpossible;
    @JsonProperty("actualjackpot")
    private Double actualjackpot;
    @JsonProperty("estimatedjackpot")
    private Double estimatedjackpot;
    @JsonProperty("guaranteedjackpot")
    private Object guaranteedjackpot;
    @JsonProperty("boostedjackpot")
    private Object boostedjackpot;
    @JsonProperty("totalprizeamount")
    private Double totalprizeamount;
    @JsonProperty("totalguaranteedjackpot")
    private Double totalguaranteedjackpot;
    @JsonProperty("jackpotwinners")
    private Double jackpotwinners;
    @JsonProperty("quickpickwinners")
    private Object quickpickwinners;
    @JsonProperty("tsresult")
    private String tsresult;
    @JsonProperty("jackpotdivisionlevelid")
    private Double jackpotdivisionlevelid;
    @JsonProperty("resultsets")
    private List<ResultSet> resultSets = null;
    @JsonProperty("resultdivisions")
    private List<ResultDivision> resultDivisions = null;
    @JsonProperty("drawid")
    private Double drawid;
    @JsonProperty("topWinAmount")
    private Double topWinAmount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("iddggamedraw")
    public Double getIddggamedraw()
    {
        return iddggamedraw;
    }

    @JsonProperty("iddggamedraw")
    public void setIddggamedraw(Double iddggamedraw)
    {
        this.iddggamedraw = iddggamedraw;
    }

    @JsonProperty("iddggameevent")
    public Double getIddggameevent()
    {
        return iddggameevent;
    }

    @JsonProperty("iddggameevent")
    public void setIddggameevent(Double iddggameevent)
    {
        this.iddggameevent = iddggameevent;
    }

    @JsonProperty("name")
    public String getName()
    {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name)
    {
        this.name = name;
    }

    @JsonProperty("rolloveramount")
    public Double getRolloveramount()
    {
        return rolloveramount;
    }

    @JsonProperty("rolloveramount")
    public void setRolloveramount(Double rolloveramount)
    {
        this.rolloveramount = rolloveramount;
    }

    @JsonProperty("isrolldownpossible")
    public Double getIsrolldownpossible()
    {
        return isrolldownpossible;
    }

    @JsonProperty("isrolldownpossible")
    public void setIsrolldownpossible(Double isrolldownpossible)
    {
        this.isrolldownpossible = isrolldownpossible;
    }

    @JsonProperty("actualjackpot")
    public Double getActualjackpot()
    {
        return actualjackpot;
    }

    @JsonProperty("actualjackpot")
    public void setActualjackpot(Double actualjackpot)
    {
        this.actualjackpot = actualjackpot;
    }

    @JsonProperty("estimatedjackpot")
    public Double getEstimatedjackpot()
    {
        return estimatedjackpot;
    }

    @JsonProperty("estimatedjackpot")
    public void setEstimatedjackpot(Double estimatedjackpot)
    {
        this.estimatedjackpot = estimatedjackpot;
    }

    @JsonProperty("guaranteedjackpot")
    public Object getGuaranteedjackpot()
    {
        return guaranteedjackpot;
    }

    @JsonProperty("guaranteedjackpot")
    public void setGuaranteedjackpot(Object guaranteedjackpot)
    {
        this.guaranteedjackpot = guaranteedjackpot;
    }

    @JsonProperty("boostedjackpot")
    public Object getBoostedjackpot()
    {
        return boostedjackpot;
    }

    @JsonProperty("boostedjackpot")
    public void setBoostedjackpot(Object boostedjackpot)
    {
        this.boostedjackpot = boostedjackpot;
    }

    @JsonProperty("totalprizeamount")
    public Double getTotalprizeamount()
    {
        return totalprizeamount;
    }

    @JsonProperty("totalprizeamount")
    public void setTotalprizeamount(Double totalprizeamount)
    {
        this.totalprizeamount = totalprizeamount;
    }

    @JsonProperty("totalguaranteedjackpot")
    public Double getTotalguaranteedjackpot()
    {
        return totalguaranteedjackpot;
    }

    @JsonProperty("totalguaranteedjackpot")
    public void setTotalguaranteedjackpot(Double totalguaranteedjackpot)
    {
        this.totalguaranteedjackpot = totalguaranteedjackpot;
    }

    @JsonProperty("jackpotwinners")
    public Double getJackpotwinners()
    {
        return jackpotwinners;
    }

    @JsonProperty("jackpotwinners")
    public void setJackpotwinners(Double jackpotwinners)
    {
        this.jackpotwinners = jackpotwinners;
    }

    @JsonProperty("quickpickwinners")
    public Object getQuickpickwinners()
    {
        return quickpickwinners;
    }

    @JsonProperty("quickpickwinners")
    public void setQuickpickwinners(Object quickpickwinners)
    {
        this.quickpickwinners = quickpickwinners;
    }

    @JsonProperty("tsresult")
    public String getTsresult()
    {
        return tsresult;
    }

    @JsonProperty("tsresult")
    public void setTsresult(String tsresult)
    {
        this.tsresult = tsresult;
    }

    @JsonProperty("jackpotdivisionlevelid")
    public Double getJackpotdivisionlevelid()
    {
        return jackpotdivisionlevelid;
    }

    @JsonProperty("jackpotdivisionlevelid")
    public void setJackpotdivisionlevelid(Double jackpotdivisionlevelid)
    {
        this.jackpotdivisionlevelid = jackpotdivisionlevelid;
    }

    @JsonProperty("resultsets")
    public List<ResultSet> getResultSets()
    {
        return resultSets;
    }

    @JsonProperty("resultsets")
    public void setResultSets(List<ResultSet> resultSets)
    {
        this.resultSets = resultSets;
    }

    @JsonProperty("resultdivisions")
    public List<ResultDivision> getResultDivisions()
    {
        return resultDivisions;
    }

    @JsonProperty("resultdivisions")
    public void setResultDivisions(List<ResultDivision> resultDivisions)
    {
        this.resultDivisions = resultDivisions;
    }

    @JsonProperty("drawid")
    public Double getDrawid()
    {
        return drawid;
    }

    @JsonProperty("drawid")
    public void setDrawid(Double drawid)
    {
        this.drawid = drawid;
    }

    @JsonProperty("topWinAmount")
    public Double getTopWinAmount()
    {
        return topWinAmount;
    }

    @JsonProperty("topWinAmount")
    public void setTopWinAmount(Double topWinAmount)
    {
        this.topWinAmount = topWinAmount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

}
