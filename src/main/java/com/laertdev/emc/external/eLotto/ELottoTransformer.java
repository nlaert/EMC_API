package com.laertdev.emc.external.eLotto;

import com.laertdev.emc.entities.Raffle;
import com.laertdev.emc.entities.RafflePrizes;
import com.laertdev.emc.entities.RaffleResult;
import com.laertdev.emc.external.RaffleInformation;
import com.laertdev.emc.external.Utils;
import com.laertdev.emc.external.eLotto.dto.GameEvent;
import com.laertdev.emc.external.eLotto.dto.MainDraw;
import com.laertdev.emc.external.eLotto.dto.ResultDivision;
import com.laertdev.emc.external.eLotto.dto.ResultSet;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static com.laertdev.emc.EmcApplication.VALUE_SEPARATOR;

class ELottoTransformer {

    private static final String ELOTTO_VALUE_SEPARATOR = ",";

    static RaffleInformation transform(GameEvent gameEvent) {
        RaffleInformation raffleInformation = new RaffleInformation();

        String drawDate = gameEvent.getDrawdate().substring(0, 10);
        LocalDate raffleDate = LocalDate.parse(drawDate);

        Raffle raffle = new Raffle();

        raffle.setDate(raffleDate);
        raffle.setYear(raffleDate.getYear());
        raffle.setNumber(Utils.calculateRaffleNumber(raffleDate));
        raffleInformation.setRaffle(raffle);

        MainDraw mainDraw = gameEvent.getMainDraw();
        List<ResultSet> resultSets = mainDraw.getResultSets();

        RaffleResult raffleResult = new RaffleResult();

        raffleResult.setNumbers(resultSets.get(0).getMainvalues().replace(ELOTTO_VALUE_SEPARATOR, VALUE_SEPARATOR));
        raffleResult.setStars(resultSets.get(1).getMainvalues().replace(ELOTTO_VALUE_SEPARATOR, VALUE_SEPARATOR));
        raffleInformation.setRaffleResult(raffleResult);

        List<RafflePrizes> prizesList = new ArrayList<>(13);

        for (ResultDivision resultDivision : mainDraw.getResultDivisions()) {
            RafflePrizes rafflePrizes = new RafflePrizes();
            String[] temp = resultDivision.getName().split("\\+");

            rafflePrizes.setPrizeOrder(resultDivision.getLevelid());
            rafflePrizes.setAmountOfNumbers(Integer.parseInt(temp[0]));
            rafflePrizes.setAmountOfStars(Integer.parseInt(temp[1]));
            rafflePrizes.setPrizeMoney(resultDivision.getPrizeamount().toString());
            rafflePrizes.setWinners(resultDivision.getTotalshares());

            prizesList.add(rafflePrizes);
        }

        raffleInformation.setRafflePrizes(prizesList);
        return raffleInformation;
    }
}
