
package com.laertdev.emc.external.eLotto.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddonResult
{

    @JsonProperty("gameevent")
    private GameEvent gameEvent;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("gameevent")
    public GameEvent getGameEvent()
    {
        return gameEvent;
    }

    @JsonProperty("gameevent")
    public void setGameEvent(GameEvent gameEvent)
    {
        this.gameEvent = gameEvent;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

}
