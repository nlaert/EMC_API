
package com.laertdev.emc.external.eLotto.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "levelid",
        "name",
        "shares",
        "prizetype",
        "prizeamount",
        "prizedescription",
        "totalshares"
})
public class ResultDivision
{

    @JsonProperty("levelid")
    private int levelid;
    @JsonProperty("name")
    private String name;
    @JsonProperty("shares")
    private int shares;
    @JsonProperty("prizetype")
    private String prizetype;
    @JsonProperty("prizeamount")
    private Double prizeamount;
    @JsonProperty("prizedescription")
    private Object prizedescription;
    @JsonProperty("totalshares")
    private int totalshares;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("levelid")
    public int getLevelid()
    {
        return levelid;
    }

    @JsonProperty("levelid")
    public void setLevelid(int levelid)
    {
        this.levelid = levelid;
    }

    @JsonProperty("name")
    public String getName()
    {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name)
    {
        this.name = name;
    }

    @JsonProperty("shares")
    public int getShares()
    {
        return shares;
    }

    @JsonProperty("shares")
    public void setShares(int shares)
    {
        this.shares = shares;
    }

    @JsonProperty("prizetype")
    public String getPrizetype()
    {
        return prizetype;
    }

    @JsonProperty("prizetype")
    public void setPrizetype(String prizetype)
    {
        this.prizetype = prizetype;
    }

    @JsonProperty("prizeamount")
    public Double getPrizeamount()
    {
        return prizeamount;
    }

    @JsonProperty("prizeamount")
    public void setPrizeamount(Double prizeamount)
    {
        this.prizeamount = prizeamount;
    }

    @JsonProperty("prizedescription")
    public Object getPrizedescription()
    {
        return prizedescription;
    }

    @JsonProperty("prizedescription")
    public void setPrizedescription(Object prizedescription)
    {
        this.prizedescription = prizedescription;
    }

    @JsonProperty("totalshares")
    public int getTotalshares()
    {
        return totalshares;
    }

    @JsonProperty("totalshares")
    public void setTotalshares(int totalshares)
    {
        this.totalshares = totalshares;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

}
