package com.laertdev.emc.external;

/**
 * This interface must be implemented if a new external API is used to get results.
 */
public interface ExternalAPIService {
    RaffleInformation getLastDrawResult() throws RaffleNotProcessedException;
}
