package com.laertdev.emc.external;

import com.laertdev.emc.entities.Raffle;
import com.laertdev.emc.entities.RafflePrizes;
import com.laertdev.emc.entities.RaffleResult;

import java.util.List;
import java.util.Objects;

public class RaffleInformation
{
    private Raffle raffle;
    private RaffleResult raffleResult;
    private List<RafflePrizes> rafflePrizes;

    public Raffle getRaffle()
    {
        return raffle;
    }

    public void setRaffle(Raffle raffle)
    {
        this.raffle = raffle;
    }

    public RaffleResult getRaffleResult()
    {
        return raffleResult;
    }

    public void setRaffleResult(RaffleResult raffleResult)
    {
        this.raffleResult = raffleResult;
    }

    public List<RafflePrizes> getRafflePrizes()
    {
        return rafflePrizes;
    }

    public void setRafflePrizes(List<RafflePrizes> rafflePrizes)
    {
        this.rafflePrizes = rafflePrizes;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        RaffleInformation that = (RaffleInformation) o;
        return Objects.equals(raffle, that.raffle) &&
                Objects.equals(raffleResult, that.raffleResult) &&
                Objects.equals(rafflePrizes, that.rafflePrizes);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(raffle, raffleResult, rafflePrizes);
    }
}
