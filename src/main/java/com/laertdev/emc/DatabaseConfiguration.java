package com.laertdev.emc;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfiguration {

    @Value("${spring.datasource.url}")
    private String dbUrl;
    @Value("${spring.datasource.username:}")
    private String dbUsername;
    @Value("${spring.datasource.password:}")
    private String dbPass;

    @Bean
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(dbUrl);
        if (!dbUsername.equals("")) {
            config.setUsername(dbUsername);
        }
        if (!dbPass.equals("")) {
            config.setPassword(dbPass);
        }
        return new HikariDataSource(config);
    }
}
