package com.laertdev.emc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmcApplication
{
    public static final String VALUE_SEPARATOR = ";";

    public static void main(String[] args)
    {
        SpringApplication.run(EmcApplication.class, args);
    }
}
