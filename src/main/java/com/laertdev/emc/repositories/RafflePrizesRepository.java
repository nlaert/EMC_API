package com.laertdev.emc.repositories;

import com.laertdev.emc.entities.RafflePrizes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RafflePrizesRepository extends JpaRepository<RafflePrizes, Integer> {
    RafflePrizes getByRaffleIdAndAmountOfNumbersAndAmountOfStars(Long raffleId, int amountOfNumbers, int amountOfStars);
}
