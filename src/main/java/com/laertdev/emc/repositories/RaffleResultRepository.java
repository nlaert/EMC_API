package com.laertdev.emc.repositories;

import com.laertdev.emc.entities.RaffleResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.Tuple;

public interface RaffleResultRepository extends JpaRepository<RaffleResult, Integer> {
    @Query("select rr from RaffleResult rr inner join Raffle r on rr.raffleId = r.id where r.year = ?1 " +
            "and r.number = ?2")
    RaffleResult getByYearAndNumber(int year, int raffleNumber);

    @Query("select rr, r from RaffleResult rr inner join Raffle r on rr.raffleId = r.id where r.year = ?1 " +
            "and r.number = ?2")
    Tuple getByYearAndNumberJoinRaffle(int year, int raffleNumber);

}
