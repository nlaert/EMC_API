package com.laertdev.emc.repositories;

import com.laertdev.emc.entities.Raffle;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RaffleRepository extends JpaRepository<Raffle, Integer>
{
    Raffle findFirstByYearOrderByDateDesc(int year);
}
