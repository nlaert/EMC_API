package com.laertdev.emc.external;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class UtilsTest {

    @Test
    public void shouldReturn1WhenGiven2019_1_1() {
        LocalDate localDate = LocalDate.of(2019, 1, 1);
        int actual = Utils.calculateRaffleNumber(localDate);
        int expected = 1;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturn102WhenGiven2019_12_20() {
        LocalDate localDate = LocalDate.of(2019, 12, 20);
        int actual = Utils.calculateRaffleNumber(localDate);
        int expected = 102;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturn101WhenGiven2019_12_17() {
        LocalDate localDate = LocalDate.of(2019, 12, 17);
        int actual = Utils.calculateRaffleNumber(localDate);
        int expected = 101;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturn105WhenGiven2019_12_31() {
        LocalDate localDate = LocalDate.of(2019, 12, 31);
        int actual = Utils.calculateRaffleNumber(localDate);
        int expected = 105;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturn1WhenGiven2020_1_3() {
        LocalDate localDate = LocalDate.of(2020, 1, 3);
        int actual = Utils.calculateRaffleNumber(localDate);
        int expected = 1;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturn1WhenGiven2020_1_7() {
        LocalDate localDate = LocalDate.of(2020, 1, 7);
        int actual = Utils.calculateRaffleNumber(localDate);
        int expected = 2;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturn19WhenGiven2020_3_6() {
        LocalDate localDate = LocalDate.of(2020, 3, 6);
        int actual = Utils.calculateRaffleNumber(localDate);
        int expected = 19;
        Assert.assertEquals(expected, actual);
    }
}